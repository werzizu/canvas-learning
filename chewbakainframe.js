window.onload = function () {
    var canvas = document.getElementById("myCan");
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');
        var img = new Image();

        // var imgW = 350;
        // var imgH = 300;

        img.onload = function () {
            // console.log("now we can do some stuff");
            ctx.drawImage(img, 20, 20, 560, 440);
            ctx.fillStyle = "rgba(1,255,255,1)";
            ctx.font = '50pt Calibri';
            var text = "AAAAAAAAAAaaaaaaaaaaaaaaargh";
            ctx.fillText(text, 50, 50, 500);

            ctx.drawImage(document.getElementById("frame"), -25, -10, 660, 520);
        };
        img.src = 'frogy.jpg';
    }
};