ctx.save();

ctx.scale(0.2,0.2);

ctx.fillStyle = "blue";
ctx.beginPath();
ctx.arc(120,60,50,0,2*Math.PI);
ctx.fill();

ctx.translate(200,200);



ctx.fillStyle = "yellow";
ctx.globalAlpha = 0.5;

ctx.beginPath();
ctx.arc(320,160,70,0,2*Math.PI);
ctx.fill();

ctx.restore();

ctx.beginPath();
ctx.arc(220,260,70,0,2*Math.PI);
ctx.fill();

ctx.restore();
var bgcol = 0;
for(var x=0; x<30;x++){
    bgcol = Math.floor(255/30*x);
    ctx.fillStyle = "rgb(" + bgcol +"," + bgcol + "," + bgcol + ")";
    ctx.fillRect(300,300,200,100);
    ctx.setTransform(1,0,0,1,x,x);
}